import initLDAPGroupsSelect from 'ee/ldap_groups_select';
import initLDAPGroupLinks from 'ee/groups/ldap_group_links';

initLDAPGroupsSelect();
initLDAPGroupLinks();
